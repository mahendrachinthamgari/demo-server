const express = require('express');

const PORT = process.env.PORT || 2000;

const app = express();

app.get('/', function (request, response) {
    response.send('Hellow world!');
})

app.listen(PORT, function (request, response) {
    console.log(`Running.. on {PORT}`);
});